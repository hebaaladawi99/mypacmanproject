function animate() {

    animationID = requestAnimationFrame(animate);
    c.clearRect(0, 0, canvas.width, canvas.height);
  
    if (keys.w.pressed && lastKey === 'w') {
      for (let i = 0; i < boundaries.length; i++) {
        const boundary = boundaries[i];
        if (
          circleCollidesWithRectangle({
            circle: {
              ...player,
              velocity: {
                x: 0,
                y: -5,
              },
            },
            rectangle: boundary,
          })
        ) {
          player.velocity.y = 0;
          break;
        } else {
          player.velocity.y = -5;
        }
      }
    } else if (keys.a.pressed && lastKey === 'a') {
      for (let i = 0; i < boundaries.length; i++) {
        const boundary = boundaries[i];
        if (
          circleCollidesWithRectangle({
            circle: {
              ...player,
              velocity: {
                x: -5,
                y: 0,
              },
            },
            rectangle: boundary,
          })
        ) {
          player.velocity.x = 0;
          break;
        } else {
          player.velocity.x = -5;
        }
      }
    } else if (keys.s.pressed && lastKey === 's') {
      for (let i = 0; i < boundaries.length; i++) {
        const boundary = boundaries[i];
        if (
          circleCollidesWithRectangle({
            circle: {
              ...player,
              velocity: {
                x: 0,
                y: 5,
              },
            },
            rectangle: boundary,
          })
        ) {
          player.velocity.y = 0;
          break;
        } else {
          player.velocity.y = 5;
        }
      }
    } else if (keys.d.pressed && lastKey === 'd') {
      for (let i = 0; i < boundaries.length; i++) {
        const boundary = boundaries[i];
        if (
          circleCollidesWithRectangle({
            circle: {
              ...player,
              velocity: {
                x: 5,
                y: 0,
              },
            },
            rectangle: boundary,
          })
        ) {
          player.velocity.x = 0;
          break;
        } else {
          player.velocity.x = 5;
        }
      }
    }
  
    // win condition
    if (pellets.length === 0) {
      cancelAnimationFrame(animationID);
      saveScore(username, score);
      youWon = isyouWon();
      playSound(5);
    }
    
    // detect collision between player and ghosts
    for (let i = ghosts.length - 1; 0 <= i; i--) {
      const ghost = ghosts[i];
      // ghosts touch player
  
      if (
        Math.hypot(ghost.position.x - player.position.x, ghost.position.y - player.position.y) <
        ghost.radius + player.radius
      ) {
        // if we touche scared ghost remove it from game
        if (ghost.scared) {
          ghosts.splice(i, 1);
          playSound(3);
        } else {
          cancelAnimationFrame(animationID);
          playSound(2);
          saveScore(username, score);
          gameOver = isgameOver();
          //console.log('you lose!');
        }
      }
    }
    // player collides with powerup
    for (let i = powerUps.length - 1; 0 <= i; i--) {
      const powerUp = powerUps[i];
      powerUp.draw();
  
      if (
        Math.hypot(powerUp.position.y - player.position.y, powerUp.position.x - player.position.x) <
        powerUp.radius + player.radius
      ) {
        powerUps.splice(i, 1);
        // make ghosts scared
        ghosts.forEach((ghost) => {
          ghost.scared = true;
  
          setTimeout(() => {
            ghost.scared = false;
          }, 5000);
        });
      }
    }
  
    //touch pallets here
    for (let i = pellets.length - 1; 0 <= i; i--) {
      const pellet = pellets[i];
      pellet.draw();
  
      if (
        Math.hypot(pellet.position.x - player.position.x, pellet.position.y - player.position.y) <
        pellet.radius + player.radius
      ) {
        //console.log('touching');
        playSound(1);
        pellets.splice(i, 1);
        score += 10;
        scoreEl.innerHTML = score;
      }
    }
  
  
    boundaries.forEach((boundary) => {
      boundary.draw();
  
      if (
        circleCollidesWithRectangle({
          circle: player,
          rectangle: boundary,
        })
      ) {
        player.velocity.x = 0;
        player.velocity.y = 0;
      }
    });
  
    player.update();
  
    ghosts.forEach((ghost) => {
      ghost.update();
  
      const collisions = [];
      boundaries.forEach((boundary) => {
        if (
          !collisions.includes('right') &&
          circleCollidesWithRectangle({
            circle: {
              ...ghost,
              velocity: {
                x: 5,
                y: 0,
              },
            },
            rectangle: boundary,
          })
        ) {
          collisions.push('right');
        }
        if (
          !collisions.includes('left') &&
          circleCollidesWithRectangle({
            circle: {
              ...ghost,
              velocity: {
                x: -5,
                y: 0,
              },
            },
            rectangle: boundary,
          })
        ) {
          collisions.push('left');
        }
        if (
          !collisions.includes('up') &&
          circleCollidesWithRectangle({
            circle: {
              ...ghost,
              velocity: {
                x: 0,
                y: -5,
              },
            },
            rectangle: boundary,
          })
        ) {
          collisions.push('up');
        }
        if (
          !collisions.includes('down') &&
          circleCollidesWithRectangle({
            circle: {
              ...ghost,
              velocity: {
                x: 0,
                y: 5,
              },
            },
            rectangle: boundary,
          })
        ) {
          collisions.push('down');
        }
      });
      if (collisions.length > ghost.prevCollisions.length) {
        ghost.prevCollisions = collisions;
      }
      if (JSON.stringify(collisions) !== JSON.stringify(ghost.prevCollisions)) {
        if (ghost.velocity.x > 0) {
          ghost.prevCollisions.push('right');
        } else if (ghost.velocity.x < 0) {
          ghost.prevCollisions.push('left');
        } else if (ghost.velocity.y < 0) {
          ghost.prevCollisions.push('up');
        } else if (ghost.velocity.y > 0) {
          ghost.prevCollisions.push('down');
        }
  
        const pathways = ghost.prevCollisions.filter((collision) => {
          return !collisions.includes(collision);
        });
  
        const direction = pathways[Math.floor(Math.random() * pathways.length)];
  
        switch (direction) {
          case 'down':
            ghost.velocity.y = ghost.speed;
            ghost.velocity.x = 0;
            break;
          case 'up':
            ghost.velocity.y = -ghost.speed;
            ghost.velocity.x = 0;
            break;
          case 'right':
            ghost.velocity.y = 0;
            ghost.velocity.x = ghost.speed;
            break;
          case 'left':
            ghost.velocity.y = 0;
            ghost.velocity.x = -ghost.speed;
            break;
        }
  
        ghost.prevCollisions = [];
      }
    });
    if (player.velocity.x > 0) player.rotation = 0;
    else if (player.velocity.x < 0) player.rotation = Math.PI;
    else if (player.velocity.y > 0) player.rotation = Math.PI / 2;
    else if (player.velocity.y < 0) player.rotation = Math.PI * 1.5;
  } // end of animate