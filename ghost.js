// class player was here

class Ghost {
  static speed = 2;
  constructor({position, velocity, color = 'red'}) {
      this.position = position;
      this.velocity = velocity;
      this.radius = 15;
      this.rad2 = 5
      this.color = color;
      this.prevCollisions = [];
      this.speed = 2;
      this.scared = false;
  }


  draw() {
      // Ghost body
      // big circle in the center
      c.beginPath();
      c.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // rectangle right
      c.beginPath();
      c.rect(this.position.x + 5, this.position.y, 10, 10);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // rectangle left
      c.beginPath();
      c.rect(this.position.x - 15, this.position.y, 10, 10);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // circle bottom right
      c.beginPath();
      c.arc(this.position.x + 10, this.position.y + 11, this.radius - 10, 0, Math.PI * 2);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // circle bottom
      c.beginPath();
      c.arc(this.position.x, this.position.y + 11, this.radius - 10, 0, Math.PI * 2);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // circle bottom left
      c.beginPath();
      c.arc(this.position.x - 10, this.position.y + 11, this.radius - 10, 0, Math.PI * 2);
      c.closePath();
      c.fill();

      c.fillStyle = this.scared ? 'blue' : this.color
      c.fill()
      c.closePath()
      // eyes
      //left
      c.fillStyle = 'white'
      c.beginPath();
      c.arc(this.position.x - this.rad2, this.position.y, this.rad2, 0, Math.PI * 2);
      c.closePath();
      c.fill();
      c.fillStyle = 'black'
      c.beginPath();
      c.arc(this.position.x + 2 - this.rad2 - 2.5, this.position.y, this.rad2 - 2.5, 0, Math.PI * 2);
      c.closePath();
      c.fill();
      //right
      c.fillStyle = 'white'
      c.beginPath();
      c.arc(this.position.x + this.rad2, this.position.y, this.rad2, 0, Math.PI * 2);
      c.closePath();
      c.fill();
      c.fillStyle = 'black'
      c.beginPath();
      c.arc(this.position.x + 12.5 - this.rad2 - 2.6, this.position.y, this.rad2 - 2.5, 0, Math.PI * 2);
      c.closePath();
      c.fill();

  }

  update() {
      this.draw();
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
  }

  static setSpeed(s) {
      Ghost.speed=s;
      console.log("Test",Ghost.speed);
      ghosts[0].speed = s;
      ghosts[1].speed = s;
      ghosts[2].speed = s;
  }
}
  const ghosts = [
      new Ghost({
          position: {
              x: Boundary.width * 7 + Boundary.width / 2,
              y: Boundary.height + Boundary.height / 2,
          },
          velocity: {
              x: Ghost.speed,
              y: 0,
          },
      }),

      new Ghost({
          position: {
              x: Boundary.width * 7 + Boundary.width / 2,
              y: Boundary.height * 3 + Boundary.height / 2,
          },
          velocity: {
              x: Ghost.speed,
              y: 0,
          },
          color: 'pink',
      }),
      new Ghost({
          position: {
              x: Boundary.width * 5 + Boundary.width / 2,
              y: Boundary.height * 3 + Boundary.height / 2,
          },
          velocity: {
              x: Ghost.speed,
              y: 0,
          },
          color: 'purple',
      }),
  ];

  console.log(ghosts);
