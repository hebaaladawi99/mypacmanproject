const map = [
    ['1', '-', '-', '-', '-', '-', '-', '-', '-', '-', '2'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', '?', '|'],
    ['|', '?', '[', ']', '?', 'b', '?', '[', ']', '?', '|'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', '?', '|'],
    ['|', '?', '^', '?', '1', ']', '?', '[', '2', '?', '|'],
    ['|', '?', '|', '?', '.', '?', '?', '?', '.', '?', '|'],
    ['|', '?', '|', '?', '?', '?', '^', '?', '?', '?', '|'],
    ['|', '?', '4', ']', '?', '[', ')', '?', 'b', '?', '|'],
    ['|', '?', '?', '?', '?', '?', '.', '?', '?', '?', '|'],
    ['|', '?', '^', '?', '^', '?', '?', '?', '^', '?', '|'],
    ['|', '?', '.', '?', '4', ']', '?', '[', '3', '?', '|'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', 'p', '|'],
    ['4', '-', '-', '-', '-', '-', '-', '-', '-', '-', '3'],
  ];

  const map2 = [
    ['1', '-', '-', '-', '-', '-', '-', '-', '-', '-', '2'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', '?', '|'],
    ['|', '?', '[', ']', '?', 'b', '?', '[', ']', '?', '|'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', '?', '|'],
    ['|', '?', '1', ']', '?', 'b', '?', '[', '2', '?', '|'],
    ['|', '?', '.', '?', '?', '?', '?', '?', '.', '?', '|'],
    ['|', '?', '?', '?', '[', '-', ']', '?', '?', '?', '|'],
    ['|', '?', 'b', '?', '?', '?', '?', '?', 'b', '?', '|'],
    ['|', '?', '?', '?', '[', '-', ']', '?', '?', '?', '|'],
    ['|', '?', '^', '?', '?', '?', '?', '?', '^', '?', '|'],
    ['|', '?', '4', ']', '?', 'b', '?', '[', '3', '?', '|'],
    ['|', '?', '?', '?', '?', '?', '?', '?', '?', 'p', '|'],
    ['4', '-', '-', '-', '-', '-', '-', '-', '-', '-', '3'],
  ];
  
  
  
  function createImage(src) {
    const image = new Image();
    image.src = src;
    return image;
  }
  
  function level(playMap){
    playMap.forEach((row, i) => {
      row.forEach((symbol, j) => {
        switch (symbol) {
          case '-':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeHorizontal.png'),
              }),
            );
            break;
          case '|':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeVertical.png'),
              }),
            );
            break;
          case '1':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeCorner1.png'),
              }),
            );
            break;
          case '2':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeCorner2.png'),
              }),
            );
            break;
          case '3':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeCorner3.png'),
              }),
            );
            break;
          case '4':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeCorner4.png'),
              }),
            );
            break;
          case 'b':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/block.png'),
              }),
            );
            break;
          case '.':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/capBottom.png'),
              }),
            );
            break;
          case '^':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/capTop.png'),
              }),
            );
            break;
          case '[':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/capLeft.png'),
              }),
            );
            break;
          case ']':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/capRight.png'),
              }),
            );
            break;
          case ':':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeConnectorRight.png'),
              }),
            );
            break;
          case '+':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeCross.png'),
              }),
            );
            break;
          case ')':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeConnectorLeft.png'),
              }),
            );
            break;
          case '*':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeConnectorTop.png'),
              }),
            );
            break;
          case ',':
            boundaries.push(
              new Boundary({
                position: {
                  x: Boundary.width * j,
                  y: Boundary.height * i,
                },
                image: createImage('./img/pipeConnectorBottom.png'),
              }),
            );
            break;
          case '?':
            pellets.push(
              new Pellet({
                position: {
                  x: Boundary.width * j + Boundary.width / 2,
                  y: Boundary.height * i + Boundary.height / 2,
                },
              }),
            );
            break;
    
          case 'p':
            powerUps.push(
              new PowerUp({
                position: {
                  x: Boundary.width * j + Boundary.width / 2,
                  y: Boundary.height * i + Boundary.height / 2,
                },
              }),
            );
            break;
          case ' ':
            break;
        }
      });
    });
  }