//plays different soundeffects
function playSound(number) {
    switch (number) {
  
      case(1):
          let chomp = document.getElementById("chomp");
          chomp.pause();
          chomp.play();
          break;
      case(2):
          let death = document.getElementById("death");
          death.pause();
          death.play();
          break;
      case(3):
          let eat_ghost = document.getElementById("eat_ghost");
          eat_ghost.pause();
          eat_ghost.play();
          break;
      case(4):
          let eat_fruit = document.getElementById("eat_fruit");
          eat_fruit.pause();
          eat_fruit.play();
          break;
      case(5):
          let pacman_win = document.getElementById("win");
          pacman_win.pause();
          pacman_win.play();
          break;
    }
  } //end of playSound