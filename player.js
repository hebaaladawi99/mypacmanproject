class Player {
    constructor({ position, velocity }) {
      this.position = position;
      this.velocity = velocity;
      this.radius = 15;
      this.radians = 0.75; // mouth opening width
      this.openRate = 0.07; // How fast the mouth is opening
    }
  
    draw() {
      c.save();
      c.translate(this.position.x, this.position.y);
      c.rotate(this.rotation);
      c.translate(-this.position.x, -this.position.y);
      c.beginPath();
      c.arc(this.position.x, this.position.y, this.radius, this.radians, Math.PI * 2 - this.radians);
      c.lineTo(this.position.x, this.position.y);
      c.fillStyle = 'yellow';
      c.fill();
      c.closePath();
      c.restore();
    }
  
    update() {
      this.draw();
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
  
      if (this.radians < 0 || this.radians > 0.75) this.openRate = -this.openRate;
  
      this.radians += this.openRate;
    }
  }

  
const player = new Player({
    position: {
      x: Boundary.width + Boundary.width / 2,
      y: Boundary.height + Boundary.height / 2,
    },
    velocity: {
      x: 0,
      y: 0,
    },
    // "Comment"
  });
  