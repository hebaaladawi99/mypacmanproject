
function bestScores(datas) {
    const bestPlayers = document.querySelector('#bestPlayers')
    const header = document.createElement('h2')
    header.innerText = "Best Players: "
    bestPlayers.appendChild(header)
    for(let i = 0; i < datas.length; i++) {
        const div = document.createElement('div')
        div.classList.add('divPlayer')
        const userName = datas[i].username.replaceAll("%20", " ")
        if(userName == username) {
            div.style.color = 'yellow'
            div.style.fontSize = '22px'
            div.style.fontWeight = 'bold'
        }
        bestPlayers.appendChild(div)
        const number = document.createElement('span')
        number.innerHTML = `${i+1}-)`
        div.append(number)
        const name = document.createElement('span')
        name.classList.add('spanPlayer')
        name.innerHTML = userName
        div.append(name);
        const scoreDiv = document.createElement('span')
        scoreDiv.classList.add('spanScore')
        scoreDiv.innerHTML = "Score : "
        div.append(scoreDiv)
        const score = document.createElement('span')
        score.classList.add('spanPlayer')
        score.innerHTML = datas[i].score
        scoreDiv.append(score)
    }   
}

function deleteRows() {
    const bestPlayers = document.querySelector('#bestPlayers')
    bestPlayers.innerHTML = ""
}


function fetchAndDecode(request) {
    return fetch("http://localhost:7777" + "/" + request).then(response => {
        return response.json();})
}

function loadBestPlayers() {
    const request = `bestplayers`;
    fetchAndDecode(request).then(datas => {
        bestScores(datas);
    })
}

function saveScore(username,score) {
    showStatePage('deleteRows')
    const request = `savescore?username=${username}&score=${score}`;
    fetchAndDecode(request).then(response => {
        if(response.code == 200) {
            setTimeout(showStatePage('showBest', 1000))
        }
    })
}