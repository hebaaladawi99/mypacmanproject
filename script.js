let showAnimation = "";
let showUsername = false;
let showLoginBox = true;
let username = "";
let state = "notLoggedIn"
let speed = 10;


const canvas = document.querySelector('canvas');
const c = canvas.getContext('2d');
const scoreEl = document.querySelector('#scoreEl');
const bestPlayers = document.querySelector('#bestPlayers');

function showStatePage(state) {

    switch(state) {
        case 'notLoggedIn':
            loginPlayer();
            break;
        case 'loggedIn':

            animate();
            break;
        case 'showBest':
            loadBestPlayers();
            break;
        case 'deleteRows':
            deleteRows();
            break;
        default:
            alert('This can not happen')
    }
}

canvas.width = innerWidth;
canvas.height = innerHeight;

//  "Hallo"
// "Hallo again"
// "Mason was here ( ͠° ͜ʖ ͠° )"
// "Helloooo"
//  test

let score = 0;

showStatePage(state);
function circleCollidesWithRectangle({ circle, rectangle }) {
  const padding = Boundary.width / 2 - circle.radius - 1;
  return (
    circle.position.y - circle.radius + circle.velocity.y <= rectangle.position.y + rectangle.height + padding &&
    circle.position.x + circle.radius + circle.velocity.x >= rectangle.position.x - padding &&
    circle.position.y + circle.radius + circle.velocity.y >= rectangle.position.y - padding &&
    circle.position.x - circle.radius + circle.velocity.x <= rectangle.position.x + rectangle.width + padding
  );
}

let animationID;


if (!showAnimation === "show") {
  animate();
}

function loginPlayer() {
    var element = document.querySelector('#scoreEl');
    const divLogin = document.createElement('div');

    divLogin.classList.add('loginBox');
    element.append(divLogin);
    divLogin.innerHTML = 'Give your username and select a level to start!';

    const inputLogin = document.createElement('input');
    inputLogin.classList.add('inputLogin');
    inputLogin.type = 'text';
    inputLogin.id = 'username';
    inputLogin.name = 'username';
    inputLogin.placeholder = 'Give your user name';
    divLogin.appendChild(inputLogin);
  
    const button = document.createElement('button');
    button.classList.add('buttonLogin');
    button.type = 'submit';
    button.id = 'buttonLogin';
    button.innerText = 'Submit';

    //divLogin.classList.add('options');
    const divButtons = document.createElement('div')
    divButtons.classList.add('options')
    divLogin.appendChild(divButtons)

    const easy = document.createElement('button');
    easy.classList.add('easy');
    easy.type = 'submit';
    easy.id = 'easy';
    easy.innerText = 'Easy';
    divButtons.appendChild(easy);

    const medium = document.createElement('button');
    medium.classList.add('medium');
    medium.type = 'submit';
    medium.id = 'medium';
    medium.innerText = 'Medium';
    divButtons.appendChild(medium);

    const hard = document.createElement('button');
    hard.classList.add('hard');
    hard.type = 'submit';
    hard.id ='hard';
    hard.innerText = 'Hard';
    divButtons.appendChild(hard);

    

    easy.addEventListener('click', ()=> {
        Ghost.setSpeed(1);
        document.getElementById('easy').style.backgroundColor='lightblue';
    })
    medium.addEventListener('click', ()=> {
        Ghost.setSpeed(2);
        document.getElementById('medium').style.backgroundColor='cyan';
    })
    hard.addEventListener('click', ()=> {
        Ghost.setSpeed(4);
        document.getElementById('hard').style.backgroundColor='salmon';

    })

    button.addEventListener('click', ()=> {
      username = inputLogin.value;
      showStatePage('loggedIn');
      showStatePage('showBest');
      })

      const divLevelButtons = document.createElement('div')
      divLevelButtons.classList.add('levelOptions')
      divLogin.appendChild(divLevelButtons)
  
      const level1 = document.createElement('button');
      level1.classList.add('level1');
      level1.type = 'submit';
      level1.id = 'level1';
      level1.innerText = 'Level 1';
      divLevelButtons.appendChild(level1);
  
      const level2 = document.createElement('button');
      level2.classList.add('level2');
      level2.type = 'submit';
      level2.id = 'level2';
      level2.innerText = 'Level 2';
      divLevelButtons.appendChild(level2);


    level1.addEventListener('click', ()=> {
        document.getElementById('level1').style.backgroundColor='violet';
        level(map);
    })
    level2.addEventListener('click', ()=> {
        document.getElementById('level2').style.backgroundColor='violet';
       level(map2);
    })

    divLogin.appendChild(button);
}

console.log(username)

