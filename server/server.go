package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

type client struct {
	Username string `json:"username"`
	Score    int    `json:"score"`
}

type response struct {
	Error   bool   `json:"error"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func checkError(w http.ResponseWriter, err error, scode int) bool {
	if err != nil {
		httpError(w, err.Error(), scode)
		return true
	}
	return false
}

// httpError writes an error json to the http response.
func httpError(w http.ResponseWriter, msg string, scode int) {
	w.WriteHeader(scode)
	w.Header().Set("Access-Control-Allow-Orgin", "*")
	json.NewEncoder(w).Encode(response{true, scode, msg})
	return
}

func createDB() error {
	if _, err := os.Stat("./users.sqlite"); os.IsNotExist(err) {
		_, err = os.Create("./users.sqlite")
		if err != nil {
			log.Fatal("sql can not be created: ", err)
		}
	}

	DB, err := sql.Open("sqlite3", "./users.sqlite")
	if err != nil {
		return err
	}

	s, err := DB.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username VARCHAR, score INTEGER)")
	if err != nil {
		return err
	}
	_, err = s.Exec()
	if err != nil {
		return err
	}
	return nil
}

func saveScore(w http.ResponseWriter, r *http.Request) {
	var response response
	rURL := r.URL.String()
	s := strings.SplitAfter(rURL, "?")
	values := strings.Split(s[1], "&")
	username := strings.Split(values[0], "=")[1]
	score, err := strconv.Atoi(strings.Split(values[1], "=")[1])

	DB, err := sql.Open("sqlite3", "./users.sqlite")
	if checkError(w, err, 500) {
		return
	}

	row, err := DB.Query(`SELECT username, score FROM users WHERE username=$1`, username)
	if err != nil {
		return
	}

	var tempUser string
	var tempScore int
	for row.Next() {
		row.Scan(&tempUser, &tempScore)
	}

	if tempUser != "" && tempScore < score {
		_, err = DB.Exec("update users set score = ? where username = ?", score, username)
		if checkError(w, err, 500) {
			return
		}
	}

	if tempUser == "" {
		s, err := DB.Prepare("INSERT INTO users (username,score) VALUES(?,?)")
		_, err = s.Exec(username, score)
		if checkError(w, err, 500) {
			return
		}
	}

	w.Header().Set("Access-Control-Allow-Orgin", "*")
	response.Error = false
	response.Code = 200
	response.Message = "OK"
	json.NewEncoder(w).Encode(response)
}

func loadBestPlayers(w http.ResponseWriter, r *http.Request) {
	DB, err := sql.Open("sqlite3", "./users.sqlite")
	if checkError(w, err, 500) {
		return
	}

	var bestPlayers []client
	rows, err := DB.Query("SELECT username,score FROM users")
	for rows.Next() {
		var player client
		rows.Scan(&player.Username, &player.Score)
		bestPlayers = append(bestPlayers, player)
	}
	sort.Slice(bestPlayers, func(a, b int) bool {
		if bestPlayers[a].Score == bestPlayers[b].Score {
			return bestPlayers[a].Username < bestPlayers[b].Username
		} else {
			return bestPlayers[a].Score > bestPlayers[b].Score
		}
	})

	var sortedBestPlayers []client
	for i := 0; i < 5; i++ {
		sortedBestPlayers = append(sortedBestPlayers, bestPlayers[i])
	}

	w.Header().Set("Access-Control-Allow-Orgin", "*")
	json.NewEncoder(w).Encode(sortedBestPlayers)
}

func main() {

	err := createDB()
	if err != nil {
		log.Fatal("Sql Database can not be initialized: ", err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/savescore", saveScore)
	router.HandleFunc("/bestplayers", loadBestPlayers)

	log.Fatal(http.ListenAndServe(":7777", router))

}
