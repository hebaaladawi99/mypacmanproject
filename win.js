function replay() {
    window.location.reload(true)
  }
  
  function generateButton(text, id) {
    const button = document.createElement('button');
    button.innerHTML = text;
    button.id = id;
  
    return button;
  }
  
  function isyouWon() {
    const popup = this.elementWithClasses('div', 'popup')
    const div = document.createElement('div', 'popup div')
  
    const text = document.createElement('div')
    text.innerText = "STAGE CLEAR!\n\nSCORE :" + score
  
    const button = this.generateButton('replay' , 'Replay')
    button.innerText = "PLAY AGAIN?";
    button.addEventListener("click", () => this.replay())
  
    div.appendChild(text)
    div.appendChild(button)
        
    popup.appendChild(div)
    document.body.appendChild(popup)
  }
  
  function isgameOver() {
    const popup = this.elementWithClasses('div', 'popup')
    const div = document.createElement('div', 'popup div')
  
    const text = document.createElement('div')
    text.innerText = "GAME OVER\n\nSCORE: " + score
  
    const button = this.generateButton('replay' , 'Replay')
    button.innerText = "PLAY AGAIN?";
    button.addEventListener("click", () => this.replay())
  
    div.appendChild(text)
    div.appendChild(button)
        
    popup.appendChild(div)
    document.body.appendChild(popup)
  }

  function elementWithClasses(elementType, classNames) {
    const element = document.createElement(elementType);
    for (let name of classNames.split(" ")) element.classList.add(name);
    return element;
  }